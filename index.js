document.getElementById('find-ip-btn').addEventListener('click', async () => {
	try {
		// Виконуємо запит за IP адресою клієнта
		const ipResponse = await fetch('https://api.ipify.org/?format=json');
		const ipData = await ipResponse.json();
		const clientIP = ipData.ip;

		// Виконуємо запит за інформацією про фізичну адресу за IP адресою клієнта
		const geoResponse = await fetch(`http://ip-api.com/json/${clientIP}`);
		const geoData = await geoResponse.json();

		// Виводимо інформацію про місцезнаходження на сторінку
		const resultDiv = document.getElementById('result');
		resultDiv.innerHTML = `
            <p><strong>Континент:</strong> ${geoData.continent}</p>
            <p><strong>Країна:</strong> ${geoData.country}</p>
            <p><strong>Регіон:</strong> ${geoData.regionName}</p>
            <p><strong>Місто:</strong> ${geoData.city}</p>
            <p><strong>Район:</strong> ${geoData.district}</p>
        `;
	} catch (error) {
		console.error('Помилка при виконанні запиту:', error);
	}
});
